package dwfe;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

import javax.annotation.PostConstruct;
import java.util.TimeZone;

@EnableConfigServer
@EnableEurekaClient
@SpringBootApplication
public class DwfeApp
{
  @Value("${spring.jpa.properties.hibernate.jdbc.time_zone}")
  private String timeZone;

  @PostConstruct
  void started()
  {
    TimeZone.setDefault(TimeZone.getTimeZone(timeZone));
  }

  public static void main(String[] args)
  {
    SpringApplication.run(DwfeApp.class, args);
  }
}
